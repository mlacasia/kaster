﻿#include <sys/types.h>
#include <sys/socket.h>
#include <dirent.h>
#include <pthread.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#define PORTNUMBER 3600
#define CLIENTNUMBER 64

struct arg{
	int client;
	int filecount;
	struct dirent *images;
	};

void sendFile(char *path, unsigned int size, int client){
	char buffer[size];
	char filesize[10];
	char fileack[3];
	sprintf(filesize, "%d", size);
	FILE *tempfile;
	tempfile = fopen(path, "rb");
	fread(buffer, 1, size, tempfile);
	send(client, filesize, 10*sizeof(char), 0);
	recv(client, fileack, 3*sizeof(char), 0);
	send(client, buffer, size, 0);
	recv(client, fileack, 3*sizeof(char), 0);
	fclose(tempfile);
}

void clientConnect(void *argument){
	struct arg *args = (struct arg *) argument;
	struct stat state;
	int currfile = 0;
	char *filepath = NULL;
	char *tmp = "/tmp/Kastercache/";
	char *filename = NULL;
	unsigned int size = 0;
	char countbuff[10];
	char fileack[3];

	sprintf(countbuff, "%d", args->filecount);
	send(args->client, countbuff, 10*sizeof(char), 0);
	recv(args->client, fileack, 3*sizeof(char), 0);
	
	
	while(currfile < args->filecount){	
		filename = (args->images + currfile)->d_name;
		filepath = strcat(tmp, filename);		
		stat(filepath, &state);		
		size = state.st_size;
		sendFile(filepath, size, args->client);
	}	
	while(1);
	
}


int main(int argc, char *argv[]){

	int filecount = 0;
	int files = 0;	
	int clientcount = 0;
	pthread_t t_ids[CLIENTNUMBER];

	int len;
	int sckt;
	int newclient;
	struct sockaddr_in client_addr[CLIENTNUMBER];
	struct sockaddr_in server;	
	DIR *tempdir;
	
	server.sin_family = AF_INET;
	server.sin_port = htons(PORTNUMBER);
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	len = sizeof(struct sockaddr_in);
		
	tempdir = opendir("/tmp/Kastercache");
	while(readdir != NULL)filecount++;
	rewinddir(tempdir);
	
	struct dirent images[filecount];
	struct dirent *image = NULL;
	while(files < filecount){
		image = readdir(tempdir);
		images[files] = *image;
		files++;
	}
	closedir(tempdir);

	struct arg *args = malloc(sizeof(struct arg));
	args->filecount = filecount;
	args->images = &images[0];
	
	sckt = socket(AF_INET, SOCK_STREAM, 0);

	bind(sckt, (struct sockaddr *) &server, len);

	/* accept connections, max number of connections is equal to maxclients */
	while(clientcount < CLIENTNUMBER){
		listen(sckt, 5);
		newclient = accept(sckt, (struct sockaddr *) &client_addr[clientcount], &len);
		args->client = newclient;
		pthread_create(&t_ids[clientcount], NULL, &clientConnect, (void *) &args);
		clientcount++;
	}

	while(1); //waits for the program to end (closed by another thread).	

}
