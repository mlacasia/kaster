#include <sys/types.h>
#include <sys/socket.h>
#include <dirent.h>
#include <pthread.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <netdb.h>

#define PORTNUMBER 3600

void recvFile(int s, int filenumber){
	char filesize[10];
	char *fileack = "OK";
	int size = 0;
	FILE *binaryimage = NULL;
	char filepath[1024];

	sprintf(filepath, "/tmp/kaster-temp/slide-%06d.png", filenumber);
	binaryimage = fopen(filepath, "wb");
	recv(s, filesize, 10*sizeof(char), 0);
	size = atoi(filesize);
	char file[size];
	send(s, fileack, 3*sizeof(char), 0);
	recv(s, file, size, 0);
	fwrite((void *)file, size, 1, binaryimage);
	fclose(binaryimage);
	send(s, fileack, 3*sizeof(char), 0);	
}


/*argument is host name*/
int main(int argc, char *argv[]){
	int n, s, len;
    char *hostname = argv[1];
	char *fileack = "OK";
	char filecount[10];
	int files;
	int currfile = 0;

    struct hostent *hp;
    struct sockaddr_in name;

    /* Look up the host's network address.*/
    hp = gethostbyname(hostname);

    /* Create a socket in the INET domain.*/
    s = socket(AF_INET, SOCK_STREAM, 0);

    /* Create the address of the server. */
    name.sin_family = AF_INET;
    name.sin_port = htons(PORTNUMBER);
    memcpy(&name.sin_addr.s_addr, hp->h_addr, hp->h_length);
    len = sizeof(struct sockaddr_in);

    /* Connect to the server. */
    connect(s, (struct sockaddr *) &name, len);
	recv(s, filecount, 10*sizeof(char), 0);
	files=atoi(filecount);
	send(s, fileack, 3*sizeof(char), 0);
	while(currfile < files){
		recvFile(s, currfile);
	}
	while(1);
	
}
